import java.util.Scanner;

public class Challenge1 {
    public static void main(String[] args) {
        menu();
    }
    public static void menu(){
        Scanner scan = new Scanner(System.in);
        System.out.println("----------------------------------------------------");
        System.out.println("kalkulator Penghitung Luas dan Volume");
        System.out.println("----------------------------------------------------");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volum");
        System.out.println("0. Tutup");
        System.out.println("----------------------------------------------------");
        System.out.println("Masukan pilihan");

        String pilihan = scan.nextLine();

        switch (pilihan) {
            case "1" : luas();
            case "2" : volume();
            case "0" : System.exit(0);
            default : {
                System.out.println("masukan pilihan yang benar");
                menu();
            }
        }
        scan.close();
    }
    public static void luas() {
        Scanner scan = new Scanner(System.in);
        System.out.println("----------------------------------------------------");
        System.out.println("Pilih bidang yang akan dihitung");
        System.out.println("----------------------------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi panjang");
        System.out.println("0. kembali ke menu sebelumnya");
        System.out.println("----------------------------------------------------");
        System.out.println("Masukan pilihan");
        String pilihanLuas = scan.nextLine();

        switch (pilihanLuas) {
            case "1":{
                System.out.println("----------------------------------------------------");
                System.out.println("Anda Memilih Persegi");
                System.out.println("----------------------------------------------------");
                System.out.println("Masukan Sisi :");
                double sisi = scan.nextDouble();
                double hasil = sisi * sisi;
                System.out.println("Luas dari Persegi adalah " + hasil);
                tekan();
            }
            case "2" :{
                System.out.println("----------------------------------------------------");
                System.out.println("Anda Meilih Lingkaran");
                System.out.println("----------------------------------------------------");
                System.out.println("Masukan jari jari :");
                double jari = scan.nextDouble();
                double hasil = 3.14 * jari;
                System.out.println("Luas dari Lingkaran adalah " + hasil);
                tekan();
            }
            case "3" : {
                System.out.println("----------------------------------------------------");
                System.out.println("Anda Meilih Segitiga");
                System.out.println("----------------------------------------------------");
                System.out.println("Masukan alas :");
                double alas = scan.nextDouble();
                System.out.println("Masukan tinggi");
                double tinggi = scan.nextDouble();
                double hasil = 0.5 * alas * tinggi;
                System.out.println("Luas dari Segitiga adalah " + hasil);
                tekan();
            }
            case "4" : {
                System.out.println("----------------------------------------------------");
                System.out.println("Anda Meilih Persegi Panjang");
                System.out.println("----------------------------------------------------");
                System.out.println("Masukan Panjang");
                double panjang = scan.nextDouble();
                System.out.println("Masukan Lebar");
                double lebar = scan.nextDouble();
                double hasil = panjang * lebar;
                System.out.println("Luas dari Persegi Panjang adalah :" + hasil);
                tekan();
            }
            case "0" : {
                menu();
            }
            default : {
                System.out.println("Masukan pilihan yang benar say");
                luas();
            }
        }

    }
    public static void volume(){
        Scanner scan = new Scanner(System.in);
        System.out.println("----------------------------------------------------");
        System.out.println("Pilih bidang yang akan dihitung");
        System.out.println("----------------------------------------------------");
        System.out.println("Menu");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. kembali ke menu sebelumnya");
        System.out.println("----------------------------------------------------");
        System.out.println("Masukan pilihan");
        String pilihanVolume = scan.nextLine();

        switch (pilihanVolume) {
            case "1" : {
                System.out.println("----------------------------------------------------");
                System.out.println("Anda Memilih Kubus");
                System.out.println("----------------------------------------------------");
                System.out.println("Masukan Sisi");
                double sisi = scan.nextDouble();
                double hasil = sisi * sisi * sisi;
                System.out.println("Volume dari Kubus adalah :" + hasil);
                tekan();
            }
            case "2" : {
                System.out.println("----------------------------------------------------");
                System.out.println("Anda Memilih Balok");
                System.out.println("----------------------------------------------------");
                System.out.println("Masukan Panjang");
                double panjang = scan.nextDouble();
                System.out.println("Masukan Lebar");
                double lebar = scan.nextDouble();
                System.out.println("Masukan Tinggi");
                double tinggi = scan.nextDouble();
                double hasil = panjang * lebar * tinggi;
                System.out.println("Volume Balok adalah :" + hasil);
                tekan();
            }
            case "3" : {
                System.out.println("----------------------------------------------------");
                System.out.println("Anda Memilih Tabung");
                System.out.println("----------------------------------------------------");
                System.out.println("Masukan Jari-Jari");
                double jari = scan.nextDouble();
                System.out.println("Masukan Tinggi");
                double tinggi = scan.nextDouble();
                double hasil = 3.14 * tinggi * jari;
                System.out.println("Volume Tabung adalah :" + hasil);
                tekan();
            }
            case "0" : menu();
            default : {
                System.out.println("Masukan pilihan yang benar bestie");
                volume();
            }
        }
    }
    public static void tekan(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Tekan apa saja untuk kembali ke menu utama");
        if(scan.nextLine().equals("c")){
            menu();
        }else {
            menu();
        }
    }

}
